# MUSTDATA项目接口定义

## 登录和个人信息模块

1.根据手机号获取短信验证码接口  
POST /api/v1/message/code  
请求参数
```javascript
{
    "mobile":"13822656261",//11位手机号
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取短信验证码成功"
    "data":243561,//6位数字验证码
}
```
2.手机号登录/注册接口  
POST /api/v1/login  
请求参数
```javascript
{
    "mobile":"13822656261",
    "code":243561
}
```
返回参数
```javascript
{
    "code":0,
    "message":"登录成功"
    "data":"TOKEN内容",//6位数字验证码
}
```

3.获取个人信息接口 
GET /api/v1/account  
请求参数
```javascript
//在header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取成功"
    "data":{
        "name":"张艳艳",
        "avatar":"头像URL地址",
        "roleId":12,
    }
}
```
4.获取角色列表
GET /api/v1/roles  
请求参数
```javascript
//在header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取成功"
    "data":[{
                "roleId":6,
                "roleName":"管理员"
            },{
                "roleId":8,
                "roleName":"游客"
            },{
                "roleId":12,
                "roleName":"运维工人"
            }]
}
```
5.修改个人信息接口 
PUT /api/v1/account  
请求参数
```javascript
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}

//在request对象的body中传参
{
    "name":"张艳艳",
    "avatar":"头像URL地址",
    "roleId":12,
}
```
返回参数
```javascript
{
    "code":0,
    "message":"修改成功"
    "data":"token内容"
}
```
6.获取用来修改绑定手机号的验证码
POST /api/v1/message/code?type=modify  
请求参数
```javascript
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
//在request对象的body中传参
{
    "mobile":"13822656261",//11位手机号
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取短信验证码成功"
    "data":243561,//6位数字验证码
}
```
7.修改绑定手机号
PUT /api/v1/account  
请求参数
```javascript
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
//在request对象的body中传参
{
    "mobile":"1332563318",//新手机号
    "code":243561 //短信验证码
}
```
返回参数
```javascript
{
    "code":0,
    "message":"修改成功"
    "data":"token内容"
}
//修改绑定的手机号以后要重新登录,自动生成重新登录的token
```
8.获取消息列表
GET /api/v1/messages?type={0}&page={1}&perPage={20}  
请求参数
```javascript
//在URL中传参
{
    type:1,//取值范围待定,默认取0,表示所有类型
    page:1,//默认取值1,分页的当前页码1,
    perPage:20,//默认取值20,每个页面最大记录数
}
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取成功"
    "data":{
        "total":1024,//当前用户所有的消息数
        "items":[
            {
                "msgId":123,
                "msgContent":"消息内容",
                "typeId":3//消息类型ID
                "typeName":"消息类型名3"
            },
            {
                "msgId":124,
                "msgContent":"消息内容2",
                "typeId":2//消息类型ID
                "typeName":"消息类型名2"
            }
        ]
    }
}
```
9.修改消息状态
PUT /api/v1/messages/{msgId}  
请求参数
```javascript
//在URL中传参{msgId}表示修改的消息的ID

//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
//在request对象的body中传参
{
    "status":2,//新手机号
}
```
返回参数
```javascript
{
    "code":0,
    "message":"修改成功"
}
```
## 系统管理模块(首页和其他全局操作)  
10.获取首页信息
GET /api/v1/home  
请求参数
```javascript
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取成功",
    "data":{
        "company":{
            "name":"公司名称",
            "logo":"公司logoURL地址",
            "employeeCount":23,
            "mboxCount":43,
            "contractPeriod":"2020-06-06 :10:00:00"//平台合约期
        },
        "reports":[{
                "id":1//报表ID
                "name":"报表名称1",
                "data":{}//报表数据
            },{
                "id":2//报表ID
                "name":"报表名称2",
                "data":{}//报表数据
            }]        
    }
}
```
11.提交自定义的首页信息
PUI /api/v1/home  
请求参数
```javascript
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
//在request对象的body中传参
{
    "ids":[
        1,22,25,28,32
    ]
}
```
返回参数
```javascript
{
    "code":0,
    "message":"自定义成功"
}
```
12.分页获取管理员操作记录  
GET /api/v1/logs{?page=1}{&perPage=20}{&employeeId=2}{&from=2020-03-01}{&to=2020-04-01}  
请求参数
```javascript
//在URL中传参
{
    page:1,//默认取值1,分页的当前页码1,
    perPage:20,//默认取值20,每个页面最大记录数
    employeeId:2,//管理员的ID作为查询条件可选,
    form:"2020-03-01",//查询时间段的开始时间
    to:"2020-04-20",//查询时间段的结束时间
}
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取成功"
    "data":{
        "total":1024,//当前用户所有的消息数
        "items":[
            {
                "logId":123,
                "employeeId":12;
                "employeeName":"管理员姓名",
                "objName":"操作对象名"
                "type":123,// 操作类型ID
                "typeName":"操作类型名称",
            },
            {
                "logId":124,
                "employeeId":12;
                "employeeName":"管理员姓名",
                "objName":"操作对象名"
                "type":123,// 操作类型ID
                "typeName":"操作类型名称",
            }
        ]
    }
}
```
13.删除操作日志  
DELETE /api/v1/logs/{logId}  
请求参数
```javascript
//在URL中传参的{logId},如果logId为,隔开的数字串,就批量删除
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"删除操作日志成功"
}
```
## 站点管理模块
14.获取站点列表  
GET /api/v1/stations{?page=1}{&perPage=20}{&name=三元朱}  
请求参数
```javascript
//在URL中传参
{
    name:"根据站点名查询的关键字",
    page:1,//默认取值1,分页的当前页码1,
    perPage:20//默认取值20,每个页面最大记录数
}
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取成功"
    "data":{
        "total":1024,//当前用户所有的消息数
        "items":[
            {
                "stationId":123,
                "name":"站点名称1",
                "image":"站点封面图片的URL"
            },
            {
                "stationId":124,
                "name":"消息内容",
                "image":"站点封面图片的URL"
            }
        ]
    }
}
```
15.获取单个站点的站点详情  
GET /api/v1/stations/{stationId}  
请求参数
```javascript
//在URL中传参中{stationId}

//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取成功"
    "data":{
        "stationId":123,
        "name":"站点名称1",
        "image":"站点封面图片的URL",
        "principal":"负责人姓名",
        "address":"公司地址",
        "intruduce":"介绍",
        "employees":[{
                "id":1,
                "name":"员工1号",
                "avatar":"用户头像url"
            },{
                "id":2,
                "name":"员工2号",
                "avatar":"用户头像url2"
            },{
                "id":3,
                "name":"员工3号",
                "avatar":"用户头像url3"
            }
        ]
    }
}
```
16.新加站点  
POST /api/v1/stations/
请求参数
```javascript
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
//在request对象的header中传参
{
    "name":"站点名称1",
    "image":"站点封面图片的URL",//图片上传接口暂定
    "address":"公司地址",
    "intruduce":"公司简介",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"添加成功"
}
```
17.修改站点基本信息  
PUT /api/v1/stations/{stationId}  
请求参数
```javascript
//在URL中传参{stationId}

//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
//在request对象的header中传参
{
    "name":"站点名称1",
    "image":"站点封面图片的URL",//图片上传接口暂定
    "address":"公司地址",
    "intruduce":"公司简介",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"修改成功"
}
```
## MBOX配置和数据配置模块
18.分页获取当前公司下所有盒子  
GET /api/v1/mboxes{?page=1}{&perPage=20}{&name=三元朱}{&stationId=1}  
请求参数
```javascript
//在URL中传参
{
    name:"根据盒子名查询的关键字",
    stationId:2,//根据所在站点查询盒子
    page:1,//默认取值1,分页的当前页码1,
    perPage:20,//默认取值20,每个页面最大记录数
}
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取成功"
    "data":{
        "total":1024,//当前用户所有的消息数
        "items":[
            {
                "boxId":123,
                "name":"站点名称1",
                "stationId":2,//所属站点ID
                "stationName":"所属站点名称",
                "image":"盒子1的预览图"
            },{
                "boxId":124,
                "name":"站点名称2",
                "stationId":2,//所属站点ID
                "stationName":"所属站点名称",
                "image":"盒子2的预览图"
            }
        ]
    }
}
```
19.获取单个盒子详情
GET /api/v1/mboxes/{boxId}
请求参数
```javascript
//在URL中传参中{boxId}

//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取成功"
    "data":{
        "boxId":123,
        "name":"Box名称1",
        "image":"BOX预览图",
        "remark":"盒子描述",
        "stationId":2,//所属站点ID
        "stationName":"所属站点名称",
        "channel":1,//通道接口可选项ID
        "protocol":1//可选的协议ID,
        "ip":"127.0.0.1",
        "port":8080,//端口号
        "points":[{//数据点列表
                "id":1,
                "name":"数据点1",
                "deviceId":1,
                "address":0xff//寄存器地址
                "funcCode":"功能码"
                "dataType":1,//数据类型可选项的ID
                "dataSize":8//数据长度
            },{
                "id":2,
                "name":"数据点1",
                "deviceId":1,
                "address":0xff//寄存器地址
                "funcCode":"功能码"
                "dataType":1,//数据类型可选项的ID
                "dataSize":8//数据长度
            },{
                "id":3,
                "name":"数据点1",
                "deviceId":1,
                "address":0xff//寄存器地址
                "funcCode":"功能码"
                "dataType":1,//数据类型可选项的ID
                "dataSize":8//数据长度
            }
        ]
    }
}
```
20.新加盒子基本信息  
POST /api/v1/boxes
请求参数
```javascript
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
//在request对象的header中传参
{
    "name":"站点名称1",
    "image":"站点封面图片的URL",//图片上传接口暂定
    "remark":"公司简介",
    "stationId":1//所属站点/现场ID,
}
```
返回参数
```javascript
{
    "code":0,
    "message":"添加成功"
}
```
21.修改盒子基本信息  
PUT /api/v1/mboxes/{boxId}  
请求参数
```javascript
//在URL中传参{boxId}

//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
//在request对象的header中传参
{
    "name":"站点名称1",
    "image":"站点封面图片的URL",//图片上传接口暂定
    "remark":"公司简介",
    "stationId":1//所属站点/现场ID,
}
```
返回参数
```javascript
{
    "code":0,
    "message":"修改成功"
}
```
22.盒子配置管理  
PUT /api/v1/mboxes/config/{boxId}
请求参数
```javascript
//在URL中传参{boxId}

//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
//在request对象的header中传参
{
    "channel":1,//通道接口可选项ID
    "protocol":1//可选的协议ID,
    "ip":"127.0.0.1",
    "port":8080,//端口号
    "points":[{//数据点列表
                "name":"数据点1",
                "deviceId":1,
                "address":0xff//寄存器地址
                "funcCode":"功能码"
                "dataType":1,//数据类型可选项的ID
                "dataSize":8//数据长度
            },{
                "name":"数据点1",
                "deviceId":1,
                "address":0xff//寄存器地址
                "funcCode":"功能码"
                "dataType":1,//数据类型可选项的ID
                "dataSize":8//数据长度
            },{
                "name":"数据点1",
                "deviceId":1,
                "address":0xff//寄存器地址
                "funcCode":"功能码"
                "dataType":1,//数据类型可选项的ID
                "dataSize":8//数据长度
            }
        ]
}
```
返回参数
```javascript
{
    "code":0,
    "message":"配置成功"
}
```
## 报表管理模块
23.分页获取数据点列表  
GET /api/v1/points-data{?page=1}{&perPage=20}{&dataName=数据名称}{&connectedStatus=1}   
请求参数
```javascript
//在URL中传参
{
    dataName:"根据数据名查询的关键字",
    connectedStatus:1,//根据通讯状态查询
    page:1,//默认取值1,分页的当前页码1,
    perPage:20,//默认取值20,每个页面最大记录数
}
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取成功"
    "data":{
        "total":1024,//当前用户所有的消息数
        "items":[
            {
                "pointId":123,//点号
                "pointName":"点名",
                "dataValue":2,//所属站点ID
                "unit":"单位",
                "connectedStatus":1,//通讯状态
                "lastTime":"2020-04-20 20:20:20",//最后一次上传时间
                "dataType":"数据类型",
                "deviceId":12345//设备ID
            },{
                "pointId":125,//点号
                "pointName":"点名",
                "dataValue":2.4,//所属站点ID
                "unit":"单位",
                "connectedStatus":1,//通讯状态
                "lastTime":"2020-04-20 20:20:20",//最后一次上传时间
                "dataType":"数据类型",
                "deviceId":1234//设备ID
            }
        ]
    }
}
```
24.获取数据报表列表以及实时数值
ws通讯 /api/v1/report{?reportName=报表名称}  
请求参数
```javascript
//在URL中传参{?reportName=报表名称}按照报表名称查询

//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取成功"
    "data":[
            {
                "pointId":123,//点号
                "pointName":"点名",
                "data":[
                    {"x":"2020-04-22 10:20:20","y1":2.2,"y2":4},
                    {"x":"2020-04-22 10:21:20","y1":2.2,"y2":4},
                    {"x":"2020-04-22 10:22:20","y1":2.2,"y2":4},
                    {"x":"2020-04-22 10:23:20","y1":2.2,"y2":4},
                    {"x":"2020-04-22 10:24:20","y1":2.2,"y2":4},
                    {"x":"2020-04-22 10:25:20","y1":2.2,"y2":4},
                    {"x":"2020-04-22 10:26:20","y1":2.2,"y2":4},
                    {"x":"2020-04-22 10:27:20","y1":2.2,"y2":4}
                ],//图表上的数据
            },{
                "pointId":124,//点号
                "pointName":"点名",
                "data":[
                    {"x":"2020-04-22 10:20:20","y":2.2},
                    {"x":"2020-04-22 10:21:20","y":2.2},
                    {"x":"2020-04-22 10:22:20","y":2.2},
                    {"x":"2020-04-22 10:23:20","y":2.2},
                    {"x":"2020-04-22 10:24:20","y":2.2},
                    {"x":"2020-04-22 10:25:20","y":2.2},
                    {"x":"2020-04-22 10:26:20","y":2.2},
                    {"x":"2020-04-22 10:27:20","y":2.2}
                ],//图表上的数据
            }
        ]
}
```
25.获取全部组态画面列表  
GET /api/v1/configuration-screens{?screenName=组态画面名称}  
请求参数
```javascript
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取成功"
    "data":[{
                "screenId":123,//点号
                "screenName":"组态画面名称",
                "background":"背景图片URL地址",
                "createdAt":"2020-04-20 20:20:20",//组态画面创建时间
            },{
                "screenId":123,//点号
                "screenName":"组态画面名称",
                "background":"背景图片URL地址",
                "createdAt":"2020-04-20 20:20:20",//组态画面创建时间
            }]
}
```
26.获取某一个组态画面详情
GET /api/v1/configuration-screens/{screenId}  
请求参数
```javascript
//在URL中传参{screenId}

//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取成功"
    "data":{
                "screenId":123,//点号
                "screenName":"组态画面名称",
                "background":"背景图片URL地址",
                "createdAt":"2020-04-20 20:20:20",//组态画面创建时间
                "points":[{
                        "pointId":1,
                        "pointName":"数据名称",
                        "xPercent":0.618,
                        "yPercent":0.252,
                        "dataValue":2.2
                    },{
                        "pointId":2,
                        "pointName":"数据名称2",
                        "xPercent":0.618,
                        "yPercent":0.252,
                        "dataValue":1.2
                    }
                ],
            }
}
```
27.组态换面的实时数据通讯的WebSocket接口  
WS  /api/v1/runtime-data  
```javascript
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
//在request对象的body中传参
//,分隔开的多个数据点号的ID的字符串
{
    "points":"12,15,18"
}
```
返回参数
```javascript
{
    "code":0,
    "message":"请求成功"
}
```
28.新加组态画面  
POST /api/v1/configuration-screens  
请求参数
```javascript
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
//在request对象的body中传参
{
    "screenName":"组态画面名称",
    "background":"背景图片URL地址",
    "points":[{
            "pointId":1,
            "pointName":"数据名称1",
            "xPercent":0.618,
            "yPercent":0.252
        },{
            "pointId":2,
            "pointName":"数据名称2",
            "xPercent":0.618,
            "yPercent":0.252
        }]
}
```
返回参数
```javascript
{
    "code":0,
    "message":"添加成功"
}
```
29.修改组态换面 
PUT /api/v1/configuration-screens/{screenId}  
请求参数
```javascript
//在URL中传参{screenId}

//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
//在request对象的body中传参
{
    "screenName":"组态画面名称",
    "background":"背景图片URL地址",
    "points":[{
            "pointId":1,
            "pointName":"数据名称1",
            "xPercent":0.618,
            "yPercent":0.252
        },{
            "pointId":2,
            "pointName":"数据名称2",
            "xPercent":0.618,
            "yPercent":0.252
        }]
}
```
返回参数
```javascript
{
    "code":0,
    "message":"修改成功"
}
```
30.删除组态换面   
DELETE /api/v1/configuration-screens/{screenId}  
请求参数
```javascript
//在URL中传参{screenId}

//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"删除成功"
}
```
31.获取单个报表详情信息  
GET /api/v1/report/{reportId}{?timeType=1}
请求参数
```javascript
//在URL中传参{reportId}报表ID以及{?timeType=1}时间类型取值1-一天,2-一周,3-一个月

//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
```
返回参数
```javascript
{
    "code":0,
    "message":"获取成功"
    "data":{
                "reportId":123,//点号
                "reportName":"点名",
                "timeTypeList":['近一天','近一周','近一个月']
                "data":[
                    {"x":"2020-04-22 10:20:20","y":2.2},
                    {"x":"2020-04-22 10:21:20","y":2.2},
                    {"x":"2020-04-22 10:22:20","y":2.2},
                    {"x":"2020-04-22 10:23:20","y":2.2},
                    {"x":"2020-04-22 10:24:20","y":2.2},
                    {"x":"2020-04-22 10:25:20","y":2.2},
                    {"x":"2020-04-22 10:26:20","y":2.2},
                    {"x":"2020-04-22 10:27:20","y":2.2}
                ],//图表上的数据
            }
}
```
32.报表置入
POST /api/v1/report/
请求参数
```javascript

//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
//在request对象的body中传参
{
    "pointId":"1",//一次置入多个报表"pointId","1,2,3"
}
```
返回参数
```javascript
{
    "code":0,
    "message":"置入成功"
}
```
32.删除置入报表
DELETE /api/v1/report/{pointId}
请求参数
```javascript
//URL中的参数{pointId}:,//一次置入多个报表"pointId","1,2,3"
//在request对象的header中传参
{
    "Authoriation":"Bearer Token内容",
}
//在URL中传参
```
返回参数
```javascript
{
    "code":0,
    "message":"置入成功"
}
```
(持续更新中...)