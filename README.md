# codedata

保存开发项目用到的临时图片和模拟数据

例如：访问地址 [https://gitlab.com/codetown/codedata/-/raw/master/mocks/home.json]  

![一人之下](./posters/n001.jpg)
![火影忍者](./posters/n002.jpg)
![叶问](./posters/n003.jpg)
![生活大爆炸](./posters/n004.jpg)
![老友记](./posters/n005.jpg)
![狐妖小红娘](./posters/n006.jpg)
![刺客伍六七](./posters/n007.jpg)
![庆余年](./posters/n008.jpg)
![法证先锋](./posters/n009.jpg)
![三国演义](./posters/n010.jpg)
![凡人修仙传](./posters/n011.jpg)
![天宝伏魔录](./posters/n012.jpg)
![剑雨](./posters/n013.jpg)
![刀背藏身](./posters/n014.jpg)
![武侠](./posters/n015.jpg)
![绣春刀](./posters/n016.jpg)
![十面埋伏](./posters/n017.jpg)
![师父](./posters/n018.jpg)
![一代宗师](./posters/n019.jpg)
![一个人的武林](./posters/n020.jpg)
